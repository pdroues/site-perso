+++
title       =   "Baccalauréat Général"
keywords    =   []
weight      =   30
+++

## Baccalauréat Général

### Spécialité
- Science de l'ingénieur
- Histoire-géographie, géopolitique et sciences politiques

### Option
- Mathématiques complementaires

### Mention
Assez bien

### Établissement
[Lycée Charles Renouvier, Prades 66500](https://charles-renouvier.mon-ent-occitanie.fr/)

### Date
2021