+++
title       =   "DUT Informatique"
keywords    =   []
weight      =   20
+++

## Diplôme universitaire de technologie (DUT) Informatique

### Spécialité
Informatique

### Établissement
[IUT Paul Sabatier, Toulouse 31000](https://iut.univ-tlse3.fr/)

### Date
2023