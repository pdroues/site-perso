+++
title       =   "BUT Informatique"
keywords    =   []
weight      =   10
+++

## Bachelor Universitaire de Technologie (BUT) Informatique

### Spécialité
Informatique

### Parcours
Déploiment d'Applications Communicantes et Sécurisées (DACS)

### Établissement
[IUT Paul Sabatier, Toulouse 31000](https://iut.univ-tlse3.fr/)

### Date
2024 (En cours)