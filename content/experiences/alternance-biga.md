+++
title       =   'Alternance BigA'
keywords    =   []
layout      =   'experiences'
weight      =   10

outils      =   ['linux workstation', 'linux server', 'hyperviseur', 'backup', 'annuaire', 'monitoring', 'conteunerisation', 'cicd', 'hpc']
competences =   ['realiser', 'optimiser', 'administrer', 'gerer', 'conduire', 'collaborer']

type        =   'Alternance'
enterprise  =   'BigA'
enterprise_link  =   'https://cbi-toulouse.fr/fr/equipe-big-a'
start_date  =   'septembre 2023'
end_date    =   'n/a'
+++

## Description
Alternance réalisé dans le cadre de ma troisième année de BUT

## Mes missions
