+++
title       =   "Stage FRNC"
keywords    =   []
layout      =   'experiences'
weight      =   30

outils      =   ['linux workstation', 'linux server', 'hyperviseur', 'backup', 'annuaire']
competences =   ['realiser', 'optimiser', 'administrer', 'gerer', 'conduire', 'collaborer']

type        =   'Stage'
enterprise  =   'Fédération des réserves Natuelle Catalanes'
enterprise_link  =   'https://www.reserves-naturelles-catalanes.org/'
start_date  =   'avril 2023'
end_date    =   'juin 2023'
+++

## Descriptif
Stage réalisé dans le cadre de la deusiseme année de BUT Informatique

## Missions
- 
-
-
-
-
